import 'package:flutter/material.dart';
import 'screens/Login.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';

// void main() {
//   runApp(const MyApp());
// }

// class MyApp extends StatelessWidget {
//   const MyApp({Key? key}) : super(key: key);


//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       theme: ThemeData(
//         primarySwatch: Colors.yellow,
//       ),
//       home: const LogInScreen(),
//     );
    
//   }
// }

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: ''),
      home: const SplashScreen(),
    );
  }
}
class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
        splash: Column (
          children: [
            Image.asset('assets/splash.jpg'),
          ],
        ),
        splashIconSize: 5000,
        duration: 600,
        backgroundColor: Colors.black,
        nextScreen: const LogInScreen()
      );
  }
}
