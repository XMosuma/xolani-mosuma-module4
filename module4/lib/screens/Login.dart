import 'package:flutter/material.dart';
import 'Dashboard.dart';
import 'package:module4/screens/SignUpPage.dart';
import 'package:module4/Theme.dart';
import 'package:module4/widgets/LogIn.dart';
import 'package:module4/widgets/Button.dart';

class LogInScreen extends StatelessWidget {
  const LogInScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: defaultPadding,
        child: Column(
          children: [
            const SizedBox(
              height: 120,
            ),
            Text(
              'LogIn Page',
              style: titleText,
            ),
            const SizedBox(
              height: 5,
            ),
            
            const SizedBox(
              height: 10,
            ),
            const logIn(),
            const SizedBox(
              height: 20,
            ),
            const Text(
              'Forgot password?',
              style: TextStyle(
                color: secondaryColor,
                fontSize: 14,
                decoration: TextDecoration.underline,
                decorationThickness: 1,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Dashboard(),
                  ),
                );
              },
              child: const PrimaryButton(
                buttonText: ('LogIn'),
              ),
              
            ),
            Row(
              children: [
                const SizedBox(
                  width: 10,
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const SignUpScreen(),
                      ),
                    );
                  },
                  child: Text(
                    'SignUp',
                    style: textButton.copyWith(
                      decoration: TextDecoration.underline,
                      decorationThickness: 1,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        
      ),
    );
  }
}
