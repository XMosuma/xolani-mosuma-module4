import 'package:flutter/material.dart';
import 'package:module4/widgets/MyDashboard.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          decoration: const BoxDecoration(
        image: DecorationImage(
            image: AssetImage("assets/Forest.jpg"), fit: BoxFit.cover),
      )),
      drawer: const NavDrawer(),
      appBar: AppBar(title: const Text('Menu'),
      ),
      
    );
  }
}




