// ignore: file_names

import 'package:flutter/material.dart';
import 'package:module4/Theme.dart';

// ignore: camel_case_types
class logIn extends StatefulWidget {
  const logIn({Key? key}) : super(key: key);

  @override
  State<logIn> createState() => _logInState();
  
}

// ignore: camel_case_types
class _logInState extends State<logIn> {
  bool _isObscure = true;
  @override
  Widget build(BuildContext context) {
    return Column(
      
      children: [
        buildInputForm('Email', false),
        buildInputForm('Password', true),
      ],
    );
  }

  Padding buildInputForm(String lable, bool pass) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: TextFormField(
        obscureText: pass ? _isObscure : false,
        decoration: InputDecoration(
            labelText: lable,
            labelStyle: const TextStyle(
              color: secondaryColor,
            ),
            focusedBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: secondaryColor),
            ),
            suffixIcon: pass
                ? IconButton(
                    onPressed: () {
                      setState(() {
                        _isObscure = !_isObscure;
                      });
                    },
                    icon: Icon(
                      _isObscure ? Icons.visibility_off : Icons.visibility,
                      color: secondaryColor,),
                  )
                : null),
      ),
    );
  }
}
