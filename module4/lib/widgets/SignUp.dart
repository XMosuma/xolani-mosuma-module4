import 'package:flutter/material.dart';
import 'package:module4/Theme.dart';

class SignUpForm extends StatefulWidget {
  const SignUpForm({Key? key}) : super(key: key);

  @override
  State<SignUpForm> createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  bool _isObscure = true;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        buildinputForm('First Name', false),
        buildinputForm('Last Name', false),
        buildinputForm('Email', false),
        buildinputForm('Phone', false),
        buildinputForm('Password', true),
        buildinputForm('Confirm Password', true),
      ],
    );
  }

  Padding buildinputForm(String hint, bool pass) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: TextFormField(
        obscureText: pass ? _isObscure : false,
        decoration: InputDecoration(
            hintText: hint,
            hintStyle: const TextStyle(color: secondaryColor),
            focusedBorder: const UnderlineInputBorder(
                borderSide: BorderSide(color: primaryColor)),
            suffixIcon: pass
                ? IconButton(
                    onPressed: () {
                      setState(() {
                        _isObscure = !_isObscure;
                      });
                    },
                    icon: _isObscure ? const Icon(
                      Icons.visibility_off, 
                      color: primaryColor,): const Icon(
                        Icons.visibility,
                      color: primaryColor,)
                  )
                : null),
      ),
    );
  }
}
