// ignore_for_file: use_key_in_widget_constructors

import 'package:flutter/material.dart';
import 'package:module4/Theme.dart';

class PrimaryButton extends StatelessWidget {
  // PrimaryButton({Key? key}) : super(key: key);
  final String buttonText;
  const PrimaryButton({required this.buttonText});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center        ,
      height: MediaQuery.of(context).size.height * 0.08,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16), color: primaryColor),
      child: Text(buttonText, style: textButton.copyWith(color: secondaryColor),),
    );
  }
}
