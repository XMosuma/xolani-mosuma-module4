import 'package:flutter/material.dart';
import 'package:module4/screens/Login.dart';
import 'package:module4/screens/UserProfile.dart';

class NavDrawer extends StatelessWidget {
  const NavDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          const DrawerHeader(
            decoration: BoxDecoration(
                color: Colors.yellow,
                
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/menu.jpg'))),
            child: Text(
              'Menu',
              style: TextStyle(color: Colors.white, fontSize: 25),
            ),
          ),
          ListTile(
            leading: const Icon(Icons.verified_user),
            title: const Text('Edit Profile'),
            onTap: () => {Navigator.push(context, MaterialPageRoute(
              builder: (context) => const UserProfile(),),)},
          ),
          ListTile(
            leading: const Icon(Icons.exit_to_app),
            title: const Text('Logout'),
            onTap: () => {Navigator.push(context, MaterialPageRoute(
              builder: (context) => const LogInScreen()))},
          ),
        ],
      ),
    );
  }
}
